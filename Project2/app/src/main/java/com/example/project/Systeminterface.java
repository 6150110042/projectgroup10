package com.example.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Systeminterface {
    public static String BASE_URL = "https://itlearningcenters.com/android/project0810/";


    @FormUrlEncoded
    @POST("register.php")
    Call<customerresponse> register(@Field("name") String nameValue,
                                @Field("email_cs") String emailValue,
                                @Field("phone_no") String phoneNoValue,
                                @Field("username") String userNameValue,
                                @Field("password") String passWordValue
                                );


    @FormUrlEncoded
    @POST("cust_login.php")
    Call<customerresponse> login(@Field("username") String userNameValue,
                                 @Field("password") String passWordValue
    );

    @FormUrlEncoded
    @GET("cust_profile.php?cust_id=3")
    Call<customerresponse> profile (@Field("cust_id") String custidValue ,
                                    @Field("name") String  NameValue,
                                    @Field("email_cs") String passWordValue,
                                    @Field("phone_no") String phoneNoValue,
                                    @Field("username") String userNameValue);


    Call<customerresponse> profile(String s, String toString, String toString1, String toString2);
}


