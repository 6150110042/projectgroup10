package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class editprofile extends AppCompatActivity {
    EditText name,phone,mail;
    Menu menu_edit;
    Systeminterface systeminterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Profile");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editprofit,menu);
        menu_edit = menu;
        menu_edit.findItem(R.id.save).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.edit:
                setTitle("Edit Profile");
                menu_edit.findItem(R.id.edit).setVisible(false);
                menu_edit.findItem(R.id.save).setVisible(true);
                return true;
            case R.id.save:
                setTitle("Profile");
                menu_edit.findItem(R.id.edit).setVisible(true);
                menu_edit.findItem(R.id.save).setVisible(false);
                Toast.makeText(getApplicationContext(), "save surcess", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }





    }

